# server-1
resource "aws_instance" "manager1" {
  ami               = "${lookup(var.aws_amis_base, var.aws_region)}"
  instance_type     = "t2.nano"
  availability_zone = "us-east-1a"
  count             = "1"

  key_name               = "${aws_key_pair.consul_auth.id}"
  vpc_security_group_ids = ["${aws_security_group.consul.id}", "${aws_security_group.consul_internet_access.id}", "${aws_security_group.swarm.id}", "${aws_security_group.visualizer.id}", "${aws_security_group.http.id}"]
  subnet_id              = "${aws_subnet.consul_1.id}"

  connection {
    user        = "ubuntu"
    private_key = "${file("~/.ssh/kartacatest.pem")}"
    timeout     = "${var.connection_timeout}"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p consul/data"
    ]
  }

  tags {
    Owner       = "${var.owner}"
    Terraform   = true
    Environment = "${var.environment}"
    Name        = "tf-manager1"
  }
}

# server-2
resource "aws_instance" "manager2" {
  ami               = "${lookup(var.aws_amis_base, var.aws_region)}"
  instance_type     = "t2.nano"
  availability_zone = "us-east-1b"
  count             = "1"

  key_name               = "${aws_key_pair.consul_auth.id}"
  vpc_security_group_ids = ["${aws_security_group.consul.id}", "${aws_security_group.consul_internet_access.id}", "${aws_security_group.swarm.id}", "${aws_security_group.visualizer.id}", "${aws_security_group.http.id}"]
  subnet_id              = "${aws_subnet.consul_2.id}"

  connection {
    user        = "ubuntu"
    private_key = "${file("~/.ssh/kartacatest.pem")}"
    timeout     = "${var.connection_timeout}"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p consul/data"
    ]
  }

  tags {
    Owner       = "${var.owner}"
    Terraform   = true
    Environment = "${var.environment}"
    Name        = "tf-manager2"
  }
}

# server-3
resource "aws_instance" "manager3" {
  ami               = "${lookup(var.aws_amis_base, var.aws_region)}"
  instance_type     = "t2.nano"
  availability_zone = "us-east-1c"
  count             = "1"

  key_name               = "${aws_key_pair.consul_auth.id}"
  vpc_security_group_ids = ["${aws_security_group.consul.id}", "${aws_security_group.consul_internet_access.id}", "${aws_security_group.swarm.id}", "${aws_security_group.visualizer.id}", "${aws_security_group.http.id}"]
  subnet_id              = "${aws_subnet.consul_3.id}"

  connection {
    user        = "ubuntu"
    private_key = "${file("~/.ssh/kartacatest.pem")}"
    timeout     = "${var.connection_timeout}"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p consul/data"
    ]
  }

  tags {
    Owner       = "${var.owner}"
    Terraform   = true
    Environment = "${var.environment}"
    Name        = "tf-manager3"
  }
}

# server-4
resource "aws_instance" "worker1" {
  ami               = "${lookup(var.aws_amis_base, var.aws_region)}"
  instance_type     = "t2.nano"
  availability_zone = "us-east-1a"
  count             = "1"

  key_name               = "${aws_key_pair.consul_auth.id}"
  vpc_security_group_ids = ["${aws_security_group.consul.id}", "${aws_security_group.consul_internet_access.id}", "${aws_security_group.swarm.id}", "${aws_security_group.visualizer.id}", "${aws_security_group.http.id}"]
  subnet_id              = "${aws_subnet.consul_1.id}"

  connection {
    user        = "ubuntu"
    private_key = "${file("~/.ssh/kartacatest.pem")}"
    timeout     = "${var.connection_timeout}"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p consul/data"
    ]
  }

  tags {
    Owner       = "${var.owner}"
    Terraform   = true
    Environment = "${var.environment}"
    Name        = "tf-worker1"
  }
}

# server-5
resource "aws_instance" "worker2" {
  ami               = "${lookup(var.aws_amis_base, var.aws_region)}"
  instance_type     = "t2.nano"
  availability_zone = "us-east-1b"
  count             = "1"

  key_name               = "${aws_key_pair.consul_auth.id}"
  vpc_security_group_ids = ["${aws_security_group.consul.id}", "${aws_security_group.consul_internet_access.id}", "${aws_security_group.swarm.id}", "${aws_security_group.visualizer.id}", "${aws_security_group.http.id}"]
  subnet_id              = "${aws_subnet.consul_2.id}"

  connection {
    user        = "ubuntu"
    private_key = "${file("~/.ssh/kartacatest.pem")}"
    timeout     = "${var.connection_timeout}"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p consul/data"
    ]
  }

  tags {
    Owner       = "${var.owner}"
    Terraform   = true
    Environment = "${var.environment}"
    Name        = "tf-worker2"
  }
}

# server-6
resource "aws_instance" "worker3" {
  ami               = "${lookup(var.aws_amis_base, var.aws_region)}"
  instance_type     = "t2.nano"
  availability_zone = "us-east-1c"
  count             = "1"

  key_name               = "${aws_key_pair.consul_auth.id}"
  vpc_security_group_ids = ["${aws_security_group.consul.id}", "${aws_security_group.consul_internet_access.id}", "${aws_security_group.swarm.id}", "${aws_security_group.visualizer.id}", "${aws_security_group.http.id}"]
  subnet_id              = "${aws_subnet.consul_3.id}"

  connection {
    user        = "ubuntu"
    private_key = "${file("~/.ssh/kartacatest.pem")}"
    timeout     = "${var.connection_timeout}"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p consul/data"
    ]
  }

  tags {
    Owner       = "${var.owner}"
    Terraform   = true
    Environment = "${var.environment}"
    Name        = "tf-worker3"
  }
}

# server-7
resource "aws_instance" "worker4" {
  ami               = "${lookup(var.aws_amis_base, var.aws_region)}"
  instance_type     = "t2.nano"
  availability_zone = "us-east-1a"
  count             = "1"

  key_name               = "${aws_key_pair.consul_auth.id}"
  vpc_security_group_ids = ["${aws_security_group.consul.id}", "${aws_security_group.consul_internet_access.id}", "${aws_security_group.swarm.id}", "${aws_security_group.visualizer.id}", "${aws_security_group.http.id}"]
  subnet_id              = "${aws_subnet.consul_1.id}"

  connection {
    user        = "ubuntu"
    private_key = "${file("~/.ssh/kartacatest.pem")}"
    timeout     = "${var.connection_timeout}"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p consul/data"
    ]
  }

  tags {
    Owner       = "${var.owner}"
    Terraform   = true
    Environment = "${var.environment}"
    Name        = "tf-worker4"
  }
}
