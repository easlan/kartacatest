terraform {
  backend "s3" {
    bucket = "net.kartaca.tf-remote-state"
    key    = "terraform_consul.tfstate"
    region = "us-east-1"
  }
}
