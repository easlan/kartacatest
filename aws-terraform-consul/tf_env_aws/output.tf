output "manager1.public_ip" {
  value = "${aws_instance.manager1.public_ip}"
}

output "manager2.public_ip" {
  value = "${aws_instance.manager2.public_ip}"
}

output "manager3.public_ip" {
  value = "${aws_instance.manager3.public_ip}"
}

output "worker1.public_ip" {
  value = "${aws_instance.worker1.public_ip}"
}

output "worker2.public_ip" {
  value = "${aws_instance.worker2.public_ip}"
}

output "worker3.public_ip" {
  value = "${aws_instance.worker3.public_ip}"
}

output "worker4.public_ip" {
  value = "${aws_instance.worker4.public_ip}"
}

