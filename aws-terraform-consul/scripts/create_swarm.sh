#!/bin/sh

# Creates Docker swarm

set -e

vms=( "tf-manager1" "tf-manager2" "tf-manager3"
      "tf-worker1" "tf-worker2" "tf-worker3" "tf-worker4" )

SWARM_MANAGER_IP=$(aws ec2 describe-instances \
  --filters Name='tag:Name,Values=tf-manager1' \
  --output text --query 'Reservations[*].Instances[*].PrivateIpAddress')
echo "tf-manager1 private ip: ${SWARM_MANAGER_IP}"

ec2_public_ip=$(aws ec2 describe-instances \
  --filters Name='tag:Name,Values=tf-manager1' \
  --output text --query 'Reservations[*].Instances[*].PublicIpAddress')
echo "tf-manager1 public ip: ${ec2_public_ip}"

ssh -oStrictHostKeyChecking=no -T \
  -i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} << EOSSH
  docker swarm init \
  --advertise-addr ${SWARM_MANAGER_IP}
EOSSH

MANAGER_SWARM_TOKEN=$(ssh -oStrictHostKeyChecking=no -T \
  -i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} \
  "docker swarm join-token -q manager")
WORKER_SWARM_TOKEN=$(ssh -oStrictHostKeyChecking=no -T \
  -i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} \
  "docker swarm join-token -q worker")

echo ${MANAGER_SWARM_TOKEN}
echo ${WORKER_SWARM_TOKEN}

# two other manager nodes
for vm in ${vms[@]:1:2}
do
  echo "adding ${vm} as manager"
  NODE_PUBLIC_IP=$(aws ec2 describe-instances \
    --filters Name="tag:Name,Values=${vm}" \
    --output text --query 'Reservations[*].Instances[*].PublicIpAddress')
  ssh -oStrictHostKeyChecking=no -T \
    -i ~/.ssh/kartacatest.pem ubuntu@${NODE_PUBLIC_IP} << EOSSH
    docker swarm join --token "${MANAGER_SWARM_TOKEN}" \
    "${SWARM_MANAGER_IP}":2377
EOSSH
done

# three worker nodes
for vm in ${vms[@]:3:4}
do
  echo "adding ${vm} as worker"
  NODE_PUBLIC_IP=$(aws ec2 describe-instances \
    --filters Name="tag:Name,Values=${vm}" \
    --output text --query 'Reservations[*].Instances[*].PublicIpAddress')
  ssh -oStrictHostKeyChecking=no -T \
    -i ~/.ssh/kartacatest.pem ubuntu@${NODE_PUBLIC_IP} << EOSSH
    docker swarm join --token "${WORKER_SWARM_TOKEN}" \
    "${SWARM_MANAGER_IP}":2377
EOSSH
done

ssh -oStrictHostKeyChecking=no -T \
  -i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} \
  docker node ls

echo "Script completed..."
