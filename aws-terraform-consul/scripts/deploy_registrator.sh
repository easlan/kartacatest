#!/bin/sh

# Installs registrator on (5) nodes in swarm

set -e

vms=( "tf-manager1" "tf-manager2" "tf-manager3"
      "tf-worker1" "tf-worker2" "tf-worker3" "tf-worker4" )

for vm in ${vms[@]}
do
  ec2_private_ip=$(aws ec2 describe-instances \
    --filters Name="tag:Name,Values=${vm}" \
    --output text --query 'Reservations[*].Instances[*].PrivateIpAddress')
  ec2_public_ip=$(aws ec2 describe-instances \
    --filters Name="tag:Name,Values=${vm}" \
    --output text --query 'Reservations[*].Instances[*].PublicIpAddress')

  ssh -oStrictHostKeyChecking=no -T \
    -i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} << EOSSH
    docker run -d \
      --name=registrator \
      --net=host \
      --volume=/var/run/docker.sock:/tmp/docker.sock \
      gliderlabs/registrator:latest \
        -internal consul://${ec2_private_ip:-localhost}:8500
EOSSH
done

echo "Script completed..."
