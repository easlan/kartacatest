#!/bin/sh

# Provision AWS infrastructure

set -e

TERRAFORM="/usr/local/bin/terraform"

echo " "
echo "*** Building AWS infrastructure ***"

cd ../tf_env_aws/

$TERRAFORM init
$TERRAFORM plan -no-color
$TERRAFORM apply -no-color

echo " "
echo "*** Building AWS infrastructure complete ***"
