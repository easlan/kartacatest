#!/bin/sh

# Deploys a cluster of (3) Consul Servers to (3) EC2 Instances

set -e

vms=( "tf-manager1" "tf-manager2" "tf-manager3"
      "tf-worker1" "tf-worker2" "tf-worker3" "tf-worker4" )

# value used by all consul clients
export ec2_server1_private_ip=$(aws ec2 describe-instances \
  --filters Name='tag:Name,Values=tf-manager1' \
  --output text --query 'Reservations[*].Instances[*].PrivateIpAddress')
echo "manager1 private ip: ${ec2_server1_private_ip}"

############################################################

for vm in ${vms[@]:0:1}
do
# deploy consul on manager1
echo " "
echo "*** Deploying consul server on ${vm} ***"

ec2_public_ip=$(aws ec2 describe-instances \
  --filters Name="tag:Name,Values=${vm}" \
  --output text --query 'Reservations[*].Instances[*].PublicIpAddress')

ssh -oStrictHostKeyChecking=no -T \
  -i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} << EOSSH
  docker run -d \
    --net=host \
    --hostname ${vm} \
    --name ${vm} \
    --env "SERVICE_IGNORE=true" \
    --env "CONSUL_CLIENT_INTERFACE=eth0" \
    --env "CONSUL_BIND_INTERFACE=eth0" \
    --volume /home/ubuntu/consul/data:/consul/data \
    --volume /home/ubuntu/consul/config:/consul/config \
    --publish 8500:8500 \
    consul:latest \
    consul agent -server -ui -client=0.0.0.0 \
      -bootstrap-expect=3 \
      -advertise='{{ GetInterfaceIP "eth0" }}' \
      -data-dir="/consul/data"

  sleep 5
  docker logs ${vm}
  docker exec -i ${vm} consul members
EOSSH
done

############################################################

# deploy consul on manager2 manager3
for vm in ${vms[@]:1:2}
do
echo " "
echo "*** Deploying consul server on ${vm} ***"

ec2_public_ip=$(aws ec2 describe-instances \
  --filters Name="tag:Name,Values=${vm}" \
  --output text --query 'Reservations[*].Instances[*].PublicIpAddress')

ssh -oStrictHostKeyChecking=no -T \
  -i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} << EOSSH
  docker run -d \
    --net=host \
    --hostname ${vm} \
    --name ${vm} \
    --env "SERVICE_IGNORE=true" \
    --env "CONSUL_CLIENT_INTERFACE=eth0" \
    --env "CONSUL_BIND_INTERFACE=eth0" \
    --volume /home/ubuntu/consul/data:/consul/data \
    --volume /home/ubuntu/consul/config:/consul/config \
    --publish 8500:8500 \
    consul:latest \
    consul agent -server -ui -client=0.0.0.0 \
      -advertise='{{ GetInterfaceIP "eth0" }}' \
      -retry-join="${ec2_server1_private_ip}" \
      -data-dir="/consul/data"

  sleep 5
  docker logs ${vm}
  docker exec -i ${vm} consul members
EOSSH
done

############################################################

# deploy consul on workers
for vm in ${vms[@]:3:4}
do
echo " "
echo "*** Deploying consul on ${vm} ***"

ec2_public_ip=$(aws ec2 describe-instances \
  --filters Name="tag:Name,Values=${vm}" \
  --output text --query 'Reservations[*].Instances[*].PublicIpAddress')

ssh -oStrictHostKeyChecking=no -T \
  -i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} << EOSSH
  docker run -d \
    --net=host \
    --hostname ${vm} \
    --name ${vm} \
    --env "SERVICE_IGNORE=true" \
    --env "CONSUL_CLIENT_INTERFACE=eth0" \
    --env "CONSUL_BIND_INTERFACE=eth0" \
    --volume /home/ubuntu/consul/data:/consul/data \
    --volume /home/ubuntu/consul/config:/consul/config \
    --publish 8500:8500 \
    consul:latest \
    consul agent -ui -client=0.0.0.0 \
      -advertise='{{ GetInterfaceIP "eth0" }}' \
      -retry-join="${ec2_server1_private_ip}" \
      -data-dir="/consul/data"

  sleep 5
  docker logs ${vm}
  docker exec -i ${vm} consul members
EOSSH
done

############################################################

# output consul ui url
ec2_public_ip=$(aws ec2 describe-instances \
  --filters Name="tag:Name,Values=${vm[0]}" \
  --output text --query 'Reservations[*].Instances[*].PublicIpAddress')
echo " "
echo "*** Consul UI: http://${ec2_public_ip}:8500/ui/ ***"
