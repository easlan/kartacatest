#!/bin/sh

# Destroy AWS Infrastructure

set -e

TERRAFORM="/usr/local/bin/terraform"

echo " "
echo "*** Destroying AWS infrastructure ***"

cd ../tf_env_aws/

$TERRAFORM init
$TERRAFORM destroy -force -no-color

echo " "
echo "*** Destroying AWS infrastructure complete ***"
