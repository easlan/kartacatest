#!/bin/sh

# Deploys a single instance Mano Marks’ Docker Swarm Visualizer to a swarm Manager node

set -e

vm="tf-manager1"

ec2_public_ip=$(aws ec2 describe-instances \
  --filters Name="tag:Name,Values=${vm}" \
  --output text --query 'Reservations[*].Instances[*].PublicIpAddress')

ssh -oStrictHostKeyChecking=no -T \
  -i ~/.ssh/kartacatest.pem ubuntu@${ec2_public_ip} << EOSSH
  docker service create \
    --name visualizer \
    --publish 5001:8080/tcp \
    --constraint node.role==manager \
    --mode global \
    --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
    --env "SERVICE_IGNORE=true" \
    manomarks/visualizer:latest
EOSSH

echo "Script completed..."
