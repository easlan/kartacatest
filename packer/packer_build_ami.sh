#!/bin/sh

# Builds Ubuntu Docker CE AMI

set -e
# Set packer binary path.  Apparently, ubuntu has /usr/sbin/packer which
# is not what we want
PACKER="/usr/local/bin/packer"

echo " "
echo "*** Provisioning Docker CE AMI ***"

$PACKER build -color=false ubuntu_docker_ce_ami.json
